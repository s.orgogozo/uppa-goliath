package goliath;

public interface CommInterface {
	
	void connect();
	void disconnect();
	void write(byte[] cmd);
	byte[] readReply();
	
}