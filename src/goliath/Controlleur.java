package goliath;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class Controlleur {

		static Brick brick;  
		static Vitesse vitesse = new Vitesse(10);
		static long time;
		static Enregistreur enregistreur = new Enregistreur(false);
		static Recherche recherche = new Recherche(false);
		static Thread thread;
		  
	public static long getTime() {
			return time;
		}


		public static void setTime(long time) {
			Controlleur.time = time;
		}


	public static  void note(int volume, int frequence,int temps) {
		brick.getSpeaker().playTone(volume, frequence,temps/2);
		pause(temps/2);
	}
	

public static void avancer(boolean estChrono,int temps) {
	brick.getMotor().turnAtPower((byte) (Motor.PORT_B+Motor.PORT_C), +vitesse.getVitesse());
	if(estChrono) {
		pause(temps);
		stop();
	}
	  
}

public static void reculer(boolean estChrono,int temps) {
	brick.getMotor().turnAtPower((byte) (Motor.PORT_B+Motor.PORT_C), -vitesse.getVitesse());
	if(estChrono) {
		pause(temps);
		stop();
	}
	  
}

public static void tournerSurSoiGauche() {
	brick.getMotor().turnAtPower((byte) (Motor.PORT_B), -vitesse.getVitesse());

	brick.getMotor().turnAtPower((byte) (Motor.PORT_C), vitesse.getVitesse());
	  
}

public static void tournerSurSoiDroite() {
	brick.getMotor().turnAtPower((byte) (Motor.PORT_B), vitesse.getVitesse());

	brick.getMotor().turnAtPower((byte) (Motor.PORT_C), -vitesse.getVitesse());
	  
}

public static void tournerDroite() {
	
	if(vitesse.getDerniereCommande()==40) {
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , -vitesse.getVitesse()/2);
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , -vitesse.getVitesse());		
	}else {
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , vitesse.getVitesse());
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , vitesse.getVitesse()/2);

	}

}


public static void tournerGauche() {
	if(vitesse.getDerniereCommande()==40) {

		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , -vitesse.getVitesse()/2);
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , -vitesse.getVitesse());
		    
	}else {

		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , vitesse.getVitesse());
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , vitesse.getVitesse()/2);
		    	
	}
	 
}

public static void pivotDroite() {
	
	if(vitesse.getDerniereCommande()==40) {
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , -vitesse.getVitesse()/2);
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , -vitesse.getVitesse());		
	}else {
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , vitesse.getVitesse()/2);
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , vitesse.getVitesse());

	}

}


public static void pivotGauche() {
	if(vitesse.getDerniereCommande()==40) {

		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , -vitesse.getVitesse());
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , 0);
		    
	}else {

		brick.getMotor().turnAtPower( (byte)(Motor.PORT_B) , vitesse.getVitesse());
		brick.getMotor().turnAtPower( (byte)(Motor.PORT_C) , 0);
		    	
	}
	 
}

public static void stop() {
	brick.getMotor().stopMotor((byte)(Motor.PORT_B+Motor.PORT_C), true);


}

public static void recherche() {
	while(true){
	 	int value = brick.getSensor().getValuePercent(Sensor.PORT_1, Sensor.TYPE_COLOR, Sensor.ULTRASONIC_CM);
	 	
    	long diff = -(getTime()-System.currentTimeMillis());
    	//System.out.println("diff�rence"+diff);
    	
    	recherche.ajouterDistance(value);
    	recherche.ajouterTemps(diff);
    	if(diff>=2250) {
    		System.out.print("fin");
    		recherche.setEtat(false);
    		stop();
    		int i = recherche.plusGrandeDistance();
    		System.out.println(i);
    		tournerSurSoiGauche();
    		pause(recherche.getTemps().get(i).intValue());
    		vitesse.setVitesse(vitesse.getDerniereVitesse());
    		avancer(false,0);
    		break;
    	}
	}
}



	public static void main(String args[]) {
    	
    	
    	
  brick = new Brick(new BluetoothComm("0016535d7b67"));
  setTime(System.currentTimeMillis());
  Chrono chrono = new Chrono();
  
   thread = new Thread(new Runnable() {

	     public void run() {
	         while(true) {
	        	 int value = brick.getSensor().getValuePercent(Sensor.PORT_1, Sensor.TYPE_COLOR, Sensor.ULTRASONIC_CM);
	        	 int contact = brick.getSensor().getValuePercent(Sensor.PORT_2, Sensor.TYPE_TOUCH, Sensor.TOUCH_TOUCH);
	        	
	        	if(value<30 || contact==100) {
	        		
	        				if(contact==100) {
	        					reculer(false,0);
	        					pause(1000);
	        					stop();
	        								}
	        				note(10, 370, 250);
		        		    setTime(System.currentTimeMillis());
		            		vitesse.setDerniereVitesse(vitesse.getVitesse());
		        	        vitesse.setVitesse(50);
		            		vitesse.setDerniereCommande(40); 
		        	        tournerSurSoiDroite();
		        	        recherche.recharger();
		        	    	recherche();  
	        	}
	        	}
	        
	        	
	         }

	});
  

	thread.start();
	
		
  JFrame frame = new JFrame("Key Listener");

  Container contentPane = frame.getContentPane();

  
  

  
  
  KeyListener listener = new KeyListener() {

@Override

public void keyPressed(KeyEvent event) {

    printEventInfo("Pr�ss�", event);
    chrono.stop();
    enregistreur.ajouterTemps(chrono.getDureeMs());
    chrono.start();
    enregistreur.ajouterCommade("P"+Integer.toString(event.getKeyCode()));
}

@Override

public void keyReleased(KeyEvent event) {

    //printEventInfo("Relach�", event);
   // brick.getMotor().stopMotor((byte)(Motor.PORT_B+Motor.PORT_C), true);
	//System.out.println(str);

	chrono.stop();
    enregistreur.ajouterTemps(chrono.getDureeMs());
    chrono.start();
    enregistreur.ajouterCommade("R"+Integer.toString(event.getKeyCode()));
    
    int code = event.getKeyCode();
    System.out.println(code);
    
    switch(code) {
    case 37: 
    	derniereCommande(vitesse.getDerniereCommande());
    	break;
    case 39:

    	derniereCommande(vitesse.getDerniereCommande());
    	break;
    }	
}

@Override

public void keyTyped(KeyEvent event) {

    printEventInfo("Touch�e", event);

}

public  void note(int volume, int frequence,int temps) {

	brick.getSpeaker().playTone(volume, frequence,temps/2);
	pause(temps/2);
}

private void derniereCommande(int e) {
	switch(e) {
    case 37: tournerDroite();
    break;
    case 39:  tournerGauche();
    break;
    case 38:  avancer(false,0);
    vitesse.setDerniereCommande(38);
    break;	
    case 32: stop();
    vitesse.setDerniereCommande(32);
    break;
    
    case 40: 
    reculer(false,0);
    vitesse.setDerniereCommande(40);
    	break;
    
	}
	
}
@SuppressWarnings("deprecation")
private void printEventInfo(String str, KeyEvent e) {

	//System.out.println(str);

    int code = e.getKeyCode();
  //  System.out.println(code);
    
    switch(code) {
    case 37: 
	tournerDroite();
    
    //vitesse.setDerniereCommande(37);
    break;	
    case 39:  tournerGauche();
    //vitesse.setDerniereCommande(39);
    break;
    case 38:  avancer(false,0);
    vitesse.setDerniereCommande(38);
    break;	
    case 32: stop();
    vitesse.setDerniereCommande(32);
    break;
	case 107: vitesse.accelerer();

	derniereCommande(vitesse.getDerniereCommande());
    break;
	case 109: vitesse.deccelerer();
	derniereCommande(vitesse.getDerniereCommande());
	
    break;
    case 40: 
    reculer(false,0);
    vitesse.setDerniereCommande(40);
    	break;
    case 65: 
    	enregistreur.setEtat(true);
    break;
    case 90: 
    	enregistreur.setEtat(false);
    break; 
    case 69: 
    	System.out.println(recherche.toString());
    	System.out.println(recherche.tempsToString());
    	
    break;
    case 87: 
    	note(10, 370, 250);
    break;
    	   	
    case 17: 
    	thread.stop();
    	break;

    case 20: 
    	thread.start();
    	break;
    	
    }

    
    
/*
    System.out.println("   Code: " + KeyEvent.getKeyText(code));

    System.out.println("   Caract: " + e.getKeyChar());

    int mods = e.getModifiersEx();


    System.out.println("    Action? " + e.isActionKey());
*/
}

  };

  JTextField textField = new JTextField();

  textField.addKeyListener(listener);

  contentPane.add(textField, BorderLayout.NORTH);

  frame.pack();

  frame.setVisible(true);
    }
    
    public static void pause(int temps) {
		try {
			Thread.sleep(temps);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    

}