package goliath;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class Enregistreur {

public boolean etat;

public ArrayList<String> commandes;
public ArrayList<Long> temps;


public ArrayList<Long> getTemps() {
	return temps;
}

public void setTemps(ArrayList<Long> temps) {
	this.temps = temps;
}

public boolean isEtat() {
	return etat;
}

public void setEtat(boolean etat) {

	
	this.etat = etat;
if(etat==true) {
	System.out.println("Enregistreur lanc�");
}else {
	System.out.println("Enregistreur arr�t�");
}
	
}

public List<String> getCommandes() {
	return commandes;
}

public void setCommandes(ArrayList<String> commandes) {
	this.commandes = commandes;
	
}

public Enregistreur(boolean etat) {
	super();
	this.etat = etat;
	this.commandes=new ArrayList<String>();

	this.temps=new ArrayList<Long>();
}

public void ajouterCommade(String commande) {
	this.commandes.add(commande);

}
public void ajouterTemps(Long temps) {
	this.temps.add(temps);

}


public String toString() {
		
		String strOutput = "Commandes [";  //Prints constructor name + left bracket
		for(int i = 0; i < this.commandes.size(); i++){     //myArr = ArrayList instance variable
			strOutput += this.commandes.get(i) + ", ";
			}
		strOutput = strOutput + "]";
		return strOutput;
}

public String tempsToString() {
	
	String strOutput = "Temps [";  //Prints constructor name + left bracket
	for(int i = 0; i < this.temps.size(); i++){     //myArr = ArrayList instance variable
		strOutput += this.temps.get(i) + ", ";
		}
	strOutput = strOutput + "]";
	return strOutput;
}





}
