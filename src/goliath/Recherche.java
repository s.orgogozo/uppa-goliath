package goliath;

import java.util.ArrayList;
import java.util.List;

public class Recherche {
	
	public ArrayList<Integer> distance;
	public ArrayList<Long> temps;
	public boolean etat;


	public ArrayList<Long> getTemps() {
		return temps;
	}

	public void setTemps(ArrayList<Long> temps) {
		this.temps = temps;
	}

	public boolean isEtat() {
		return etat;
	}
	
	public void setEtat(boolean etat) {

		
		this.etat = etat;
	if(etat==true) {
		System.out.println("Recherche lanc�e");
	}else {
		System.out.println("Recherche arr�t�e");
	}
		
	}

	public List<Integer> getdistance() {
		return distance;
	}

	public void setdistance(ArrayList<Integer> distance) {
		this.distance = distance;
		
	}

	public Recherche(boolean etat) {
		super();
		this.etat = etat;
		this.distance=new ArrayList<Integer>();

		this.temps=new ArrayList<Long>();
	}
	
	public void recharger() {
		this.distance=new ArrayList<Integer>();

		this.temps=new ArrayList<Long>();		
	}
	public void ajouterDistance(Integer distance) {
		this.distance.add(distance);

	}
	public void ajouterTemps(Long temps) {
		this.temps.add(temps);

	}
	
	public int plusGrandeDistance() {

	
		 ArrayList<Integer> a = (ArrayList<Integer>) this.getdistance();
		
		int max = a.get(0);
		for(int i = 1; i < a.size();i++)
		{
			if(a.get(i) > max)
			{
				max = i;
			}
		}
		System.out.println(a.get(max));
		System.out.println(this.getTemps().get(max));
		
		return max;
		
	}

	public String toString() {
			
			String strOutput = "distance [";  //Prints constructor name + left bracket
			for(int i = 0; i < this.distance.size(); i++){     //myArr = ArrayList instance variable
				strOutput += this.distance.get(i) + ", ";
				}
			strOutput = strOutput + "]";
			return strOutput;
	}

	public String tempsToString() {
		
		String strOutput = "Temps [";  //Prints constructor name + left bracket
		for(int i = 0; i < this.temps.size(); i++){     //myArr = ArrayList instance variable
			strOutput += this.temps.get(i) + ", ";
			}
		strOutput = strOutput + "]";
		return strOutput;
	}





}
