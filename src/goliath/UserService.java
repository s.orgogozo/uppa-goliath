package goliath;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/commandes")
public class UserService {
	static Brick brick;
	static Vitesse vitesse = new Vitesse(10);

	static long time;
	static Enregistreur enregistreur = new Enregistreur(false);
	  static Recherche recherche = new Recherche(false);
	  static Thread thread;
	  static Controlleur controlleur;
	  
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser( @PathParam("id") int id ) {
        User user = new User(id, "jdoe", 22);
        return user;
    }    
    @GET
    @Path("/connexion")
    @Produces(MediaType.APPLICATION_JSON)
    public String Connect() {
    //brick = new Brick(new BluetoothComm("0016535d7b67"));
    controlleur = new Controlleur();
    brick = new Brick(new BluetoothComm("0016535d7b67"));
	return "Connect�";
    }
    
    @GET
    @Path("/avancer/{temps}")
    @Produces(MediaType.APPLICATION_JSON)
    public String Avancer( @PathParam("temps") int temps ) {
        brick = new Brick(new BluetoothComm("0016535d7b67"));
    	avancer(true,temps);
    //brick = new Brick(new BluetoothComm("0016535d7b67"));
    return "le robot avancera de "+temps;
    }
    
    
    public static void avancer(boolean estChrono,int temps) {
    	brick.getMotor().turnAtPower((byte) (Motor.PORT_B+Motor.PORT_C), +vitesse.getVitesse());
    	if(estChrono) {
    		pause(temps);
    		stop();
    	}
    	  
    }
    public static void pause(int temps) {
		try {
			Thread.sleep(temps);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    public static void stop() {
    	brick.getMotor().stopMotor((byte)(Motor.PORT_B+Motor.PORT_C), true);


    }

}